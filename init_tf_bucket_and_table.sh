#!/bin/bash

aws_region=us-east-1
aws_profile=zinga-kongo

state_bucket_and_table_name=zinga-kongo-terraform-state

state_bucket=${state_bucket_and_table_name}
state_dynamodb_table=${state_bucket_and_table_name}

aws s3 mb s3://${state_bucket} \
    --region "${aws_region}" \
    --profile "${aws_profile}"

aws dynamodb create-table --cli-input-json file://table_schema.json --profile "${aws_profile}"