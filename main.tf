terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  backend "s3" {
    bucket         = "zinga-kongo-terraform-state"
    key            = "zinga-kongo-deployment"
    region         = "us-east-1"
    dynamodb_table = "zinga-kongo-terraform-state"
    profile = "zinga-kongo"
  }
}

# Configure the AWS Provider
provider "aws" {
    profile = "zinga-kongo"
    region = "us-east-1"
}

# Creat a S3 bucket 
resource "aws_s3_bucket" "zinga_kongo_wordpress_code" {
  bucket = "zinga-kongo-wordpress-code"
}

 # Add the wordpress binary to the wordpress-code bucket
resource "aws_s3_bucket_object" "wordpress_object" {
  bucket = aws_s3_bucket.zinga_kongo_wordpress_code.bucket
  key    = "wordpress-5.7.tar"
  source = "/Users/moise/Workspace/zinga-kongo-infra/wordpress-5.7.tar"
}

# Creat a security group which would allow port 22, 80 ad 443
resource "aws_security_group" "webDMZ" {
  name        = "webDMZ"
  description = "Allow inbound traffic"
  vpc_id      = data.aws_vpc.selected.id

  ingress {
    description = "SSH Config"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP config"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # ingress {
  #   description = "TLS config"
  #   from_port   = 443
  #   to_port     = 443
  #   protocol    = "tcp"
  #   cidr_blocks = ["0.0.0.0/0"]

  # }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "webDMZ"
  }
}


# Creat a security group which would allow port 22, 80 ad 443
resource "aws_security_group" "rds_launch_wizard" {
  name        = "rdsLaunchWizard"
  description = "Allow inbound traffic"
  vpc_id      = data.aws_vpc.selected.id


  ingress {
    description = "MySQL Aurora"
    from_port   = 3206
    to_port     = 3206
    protocol    = "tcp"
    security_groups = [aws_security_group.webDMZ.id]
  }

  ingress {
    description = "SSH Config"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.webDMZ.id]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "rdsLaunchWizard"
  }
}

resource "aws_iam_role" "s3ForWordpress" {
  name = "s3ForWordpress"
  
  assume_role_policy = jsonencode({
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
  })
}


resource "aws_iam_policy" "s3_all_access" {
  name        = "s3_all_access"
  description = "S3 All Access"

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
          {
              "Effect": "Allow",
              "Action": "s3:*",
              "Resource": "*"
          }
      ]
  })
}

resource "aws_iam_policy_attachment" "s3_wordpress_all_access" {
  name       = "s3_wordpress_all_access"
  roles      = [aws_iam_role.s3ForWordpress.name]
  policy_arn = aws_iam_policy.s3_all_access.arn
}

resource "aws_iam_instance_profile" "ec2_s3_profile" {
  name = "ec2_s3_profile"
  role = aws_iam_role.s3ForWordpress.name
}

# Creating a keypair
resource "tls_private_key" "private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "newkey" {
  key_name   = "tfkey"
  public_key = tls_private_key.private_key.public_key_openssh
}

resource "local_file" "key_file" {
  content  = tls_private_key.private_key.private_key_pem
  filename = "tfkey.pem"
}


# Launching EC2 instance with our security group and key pair created above
resource "aws_instance" "my_web_instance" {
  ami               = data.aws_ami.amazon_linux_2.id
  instance_type     = "t2.micro"
  availability_zone = data.aws_availability_zone.selected.name
  key_name          = aws_key_pair.newkey.key_name
  security_groups   = [aws_security_group.webDMZ.id]
  subnet_id = var.public_subnet
  iam_instance_profile = aws_iam_instance_profile.ec2_s3_profile.id

  connection {
    type        = "ssh"
    port        = 22
    user        = "ec2-user"
    private_key = tls_private_key.private_key.private_key_pem
    host        = aws_instance.my_web_instance.public_ip
  }

  provisioner "remote-exec" {
    inline = [
        "#!/bin/bash",
        "sudo yum update -y",
        "sudo yum install httpd php php-mysql -y", 
        "cd /var/www/html",
        "sudo echo 'healthy' > healthy.html",
        "sudo wget https://wordpress.org/wordpress-5.1.1.tar.gz",
        "sudo tar -xzf wordpress-5.1.1.tar.gz",
        "sudo cp -r wordpress/* /var/www/html/",
        "sudo rm -rf wordpress",
        "sudo rm -rf wordpress-5.1.1.tar.gz",
        "sudo chmod -R 755 wp-content",
        "sudo chown -R apache:apache wp-content",
        "sudo wget https://s3.amazonaws.com/bucketforwordpresslab-donotdelete/htaccess.txt",
        "sudo mv htaccess.txt .htaccess",
        "sudo chkconfig httpd on",
        "sudo service httpd start"
    ]
  }

  tags = {
    Name = "wordpress"
  }
}


# Launching a EBS and mounting it

resource "aws_ebs_volume" "my_tf_vol" {
  availability_zone = aws_instance.my_web_instance.availability_zone
  size              = 1

  tags = {
    name = "tf_vol"
  }
}

resource "aws_volume_attachment" "my_tf_vol_attach" {
  device_name  = "/dev/sdf"
  volume_id    = aws_ebs_volume.my_tf_vol.id
  instance_id  = aws_instance.my_web_instance.id
  force_detach = true
}


# Mounting The EBS to the EC2 and downloading the wordpress binary inside the instance
resource "null_resource" "ebs_mount" {

  depends_on = [
    aws_volume_attachment.my_tf_vol_attach, aws_instance.my_web_instance
  ]
  connection {
    type        = "ssh"
    port        = 22
    user        = "ec2-user"
    private_key = tls_private_key.private_key.private_key_pem
    host        = aws_instance.my_web_instance.public_ip
  }
#   provisioner "remote-exec" {

#     inline = [
#       "#!/bin/bash",
#       "yum update -y",
#       "yum install httpd php php-mysql -y", 
#       "cd /var/www/html",
#       "echo 'healthy' > healthy.html",
#       "wget https://wordpress.org/wordpress-5.1.1.tar.gz",
#       "tar -xzf wordpress-5.1.1.tar.gz",
#       "cp -r wordpress/* /var/www/html/",
#       "rm -rf wordpress",
#       "rm -rf wordpress-5.1.1.tar.gz",
#       "chmod -R 755 wp-content",
#       "chown -R apache:apache wp-content",
#       "wget https://s3.amazonaws.com/bucketforwordpresslab-donotdelete/htaccess.txt",
#       "mv htaccess.txt .htaccess",
#       "chkconfig httpd on",
#       "service httpd start"
#     ]
#   }
}


