variable "vpc_id" {
    default = "vpc-642b9b19"
}

variable "public_subnet" {
    default = "subnet-ef355289"
}

variable "private_subnet" {
  default = "subnet-55241318"
}