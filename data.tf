data "aws_region" "current" {}

data "aws_availability_zone" "selected" {
  name = "us-east-1a"
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

# Get latest Amazon Linux 2 AMI
data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}